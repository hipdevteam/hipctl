import yaml
import sys
from os.path import expanduser

def getConfig(args):
    try:
        all_config = getAllConfig()
        if args.name == 'global':
            config = getDefaults(args.name)
        elif args.name in all_config:
            config = mergeDefaults(args.name, all_config[args.name])
        else:
            config = getDefaults(args.name)
    except:
        print ( "Error retrieving configuration for '" + args.name + "'." )
        sys.exit(1)

    args_dict = vars(args)

    # channel may or may not be in config at this point
    if 'channel' in args_dict:
        if args_dict['channel'] != None:
            config['channel'] = args_dict['channel']

    for prop in config:
        if prop in args_dict:
            if args_dict[prop] != None:
                config[prop] = args_dict[prop]

    return config

def getDefaults(name):

    all_config = getAllConfig()
    config = all_config['global']

    defaults = {
        'host': config['host'],
        'path': '/usr/local/share/clients/' + name,
        'staging': name + '-staging',
        'production': name + '-production',
        'user': config['user'],
        'repo': 'git@gitlab.com:hipdevteam/' +  name,
        'ref': 'master'
    }

    if "channel" in config:
        defaults['channel'] = config['channel']

    return defaults

def mergeDefaults(name, config):
    defaults = getDefaults(name)
    keys = defaults.keys()
    for key in keys:
        config.setdefault(key, defaults[key])

    return config

def checkConfig(name):
    all_config = getAllConfig()
    if name in all_config:
        return True

    return False

def getAllConfig():
    home = expanduser("~")
    with open(home + '/.config/hipctl', 'r') as stream:
        return yaml.safe_load(stream)

def addConfig(config, name):
    all_config = getAllConfig()
    all_config[name] = config

    home = expanduser("~")
    with open(home + '/.config/hipctl', 'w') as stream:
        yaml.dump(all_config, stream)
