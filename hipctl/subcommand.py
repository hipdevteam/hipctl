from hipctl import configuration
import subprocess

def deploy(args):
    config = configuration.getConfig(args)
    command_string = "lockfile -r2 ~/hipctl.lock"
    command_string += " && cd " + config['path']
    command_string += " && git fetch origin"
    command_string += " && git checkout " + config['ref']
    command_string += " && git pull origin " + config['ref']
    command_string += " && nixops deploy -d " + config[args.env]

    if 'channel' in config:
        if config['channel'] != None:
            command_string += " -Inixpkgs=channel:" + config['channel']

    ssh = subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'], command_string] )
    removeLockfile(config['user'], config['host'])

def destroy(args):
    config = configuration.getConfig(args)
    command_string = "lockfile -r2 ~/hipctl.lock"
    command_string += " && nixops destroy -d " + config['staging']
    command_string += " && nixops destroy -d " + config['production']
    command_string += " && nixops delete -d " + config['staging']
    command_string += " && nixops delete -d " + config['production']
    command_string += " && rm -rf " + config['path']

    ssh = subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'], command_string] )
    removeLockfile(config['user'], config['host'])

def prepare(args):
    if ( configuration.checkConfig(args.name) ):
        print ('Configuration already found. Nothing to prepare.')
        return False

    config = configuration.getConfig(args)
    if ( remoteDirExists(config) ):
        print ('Remote directory already exists. Manually add it to configuration file.')
        return False

    configuration.addConfig(config, args.name)
    command_string = "lockfile -r2 ~/hipctl.lock"
    command_string += " && git clone " + config['repo'] + " " + config['path']
    command_string += " && cd " + config['path']
    command_string += " && nixops create ./staging.nix -d " + config['staging']
    command_string += " && nixops create ./production.nix -d " + config['production']

    ssh = subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'], command_string] )
    removeLockfile(config['user'], config['host'])

def secrets(args):
    config = configuration.getConfig(args)
    remote = config['user'] + '@' + config['host'] + ':' + config['path']

    if args.direction == 'push':
        subprocess.call( ["mkdir", "-p", "./secrets"] )
        subprocess.call( ["rsync", "-uav", "--delete", "./secrets", remote] )
    else:
        subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'],
                          "mkdir -p " + config['path'] + "/secrets"] )
        subprocess.call( ["rsync", "-uav", "--delete", remote + "/secrets", "./"] )

def nixops(args):
    args.name = 'global'
    config = configuration.getConfig(args)
    args_string = " ".join(args.nixopts)
    command_string = "nixops " + args_string.lstrip("-- ")
    ssh = subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'], command_string] )

def remoteDirExists(config):
    command_string = "if test -d " + config['path'] + "; then exit 1; else exit 0; fi"
    ssh = subprocess.call( ["ssh", "-t", config['user'] + "@" + config['host'], command_string] )
    if ( ssh == '1' ):
         return True

    return False

def removeLockfile(user, host):
    ssh = subprocess.call( ["ssh", "-t", user + '@' + host, "rm -f ~/hipctl.lock"] )
