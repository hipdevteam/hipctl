# Hip Control CLI
Perform common tasks on a shared build server. Made specifically with the needs
of Hip Creative and BuildingOnline in mind. No one else should be using it, but
I'm making it public anyway.

## Assumptions
There are a few assumptions baked in to this tool. These are conventions used at
Hip and may not be applicable to every context or every team. A couple of big
ones:

* Every application/websites has a set of nixops configuration files kept in a
git repository. This might be the same repository as source code, but doesn't
have to be.
* There will be two deployment configurations in each repo. One staging. One
production. Each will have a single entrypoint.
* There will be a shared build server in which will have access to a shared
account that owns the NixOps state database and has access to the remote git
repositories and any secrets/keys it needs.
* There will be a configuration file in ~/.config/hipctl that must be created
before running hipctl for the first time. More details on that below.
* The lockfile program -- part of procmail -- must be installed on build server
to handle locking.

## Configuration
The configuration file should be stored at ~/.config/hipctl. Currently, that
location cannot be changed. It is in yaml syntax.

There is a single entry that must be created manually before the tool will work.
This is the "global" option and it must contain the following two properties:

```
global:
  host: build.server.com
  user: build-user
```

This block defines the default build server host and user, but can be overridden
in configuration blocks for specific sites, or using commandline arguments.

An example site configuration would be stored as:

```
example.com:
  host: build.example.com
  path: /usr/local/share/example.com
  staging: example.com-staging
  production: example.com-production
  user: builder
  repo: gitlab.com/ourteam/example.com
  channel: nixos-22.05
```

All properties are optional. The defaults are currently hardcoded in
"/hipctl/configuration.py". If `channel` is not provided, then the default
packages determined by the build server will be used.

You can also provide `channel` as a property of the `global` option. If a global
channel is provided, it will be the default for all sites.

## Command Line Interface
```
usage: hipctl [-h] [--host HOST] [--user USER] [--production PRODUCTION]
              [--staging STAGING] [--path PATH] [--repo REPO]
              {prepare,deploy,destroy,nixops} ...

Perform common tasks on build server.

positional arguments:
  {prepare,deploy,destroy,nixops}
    prepare             Prepare a repository for deployment
    deploy              Deploy a network
    destroy             Delete, destroy, and remove deployment
    nixops              Execute any nixops command on build server

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Build server hostname or address
  --user USER           User on the build server to run commands as
  --production PRODUCTION
                        name of deployment used for production
  --staging STAGING     name of deployment used for staging
  --path PATH           path to deployment configuration on build server
  --repo REPO           git repo where deployment configuration is kept
```

### hipctl prepare
Prepare a NixOps configuration for deployment.

This command does the following:

- Adds network/site name to ~/.config/hipctl
- Clones the git repository to the build server
- creates new deployment state records in NixOps’s database for staging and
production

```
usage: hipctl prepare [-h] name

positional arguments:
  name        Name of network/site to deploy

optional arguments:
  -h, --help  show this help message and exit
```

### hipctl deploy
Deploy a NixOps network configuration.

```
usage: hipctl deploy [-h] [--ref REF] name {staging,production}

positional arguments:
  name                  Name of network/site to deploy
  {staging,production}  Environment to deploy to (staging or production)

optional arguments:
  -h, --help            show this help message and exit
  --ref REF             Git reference to use for deployment. Default: master
  --channel CHANNEL     Channel to use for nixpkgs. Optional.
```

### hipctl destroy
Destroy all running resources on staging and production, delete both networks,
and remove the repository from the build server.

```
usage: hipctl destroy [-h] name

positional arguments:
  name        Name of network/site to destroy

optional arguments:
  -h, --help  show this help message and exit
```

### hipctl secrets
Sync `/secrets` directory between local machine and build server.

```
usage: hipctl secrets [-h] {push,pull} name

positional arguments:
  {push,pull}  Wether to push to server or pull to localhost
  name         Name of network/site

optional arguments:
  -h, --help   show this help message and exit
```

### hipctl nixops
Run any nixops command on the build server.

```
hipctl nixops -- info -d spibapp-staging
```

```
usage: hipctl nixops [-h] nixopts

positional arguments:
  nixopts     String to pass as arguments to nixops

optional arguments:
  -h, --help  show this help message and exit
```

## Build and Install
To create a development environment:
```
$ nix-shell
```

To build without adding to global environment:
```
$ nix-build
```

To install in your environment:
```
$ nix-env -f default.nix -i
```
