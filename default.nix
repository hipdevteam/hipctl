{pkgs ? import <nixpkgs> { inherit system; }
, system ? builtins.currentSystem }:

pkgs.python3Packages.buildPythonPackage rec {
  version = "0.0.10";
  name = "hipctl-${version}";
  namePrefix = "";

  src = ./.;

  buildInputs = [ pkgs.python3 ];

  propagatedBuildInputs = with pkgs.python3Packages; [ pyyaml pkgs.openssh pkgs.rsync ];

  meta.description = "Perform common tasks on a shared build server.";
}
