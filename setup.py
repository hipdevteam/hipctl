from distutils.core import setup

setup(name='hipctl',
      version='0.0.10',
      description='Perform common tasks on Hip Build Server',
      author='Zach',
      author_email='zach@hipcreativeinc.com',
      scripts=['scripts/hipctl'],
      packages=['hipctl']
)
